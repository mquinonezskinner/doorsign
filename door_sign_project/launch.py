__author__ = 'mqs'

import sys
import Lcd
import time
import RPi.GPIO as io


def main():
    try:
        pir_pin = 7
        io.setmode(io.BOARD)
        io.setup(pir_pin, io.IN)
        delay = 10
        machine = 1
        lcd = Lcd.NeoSec(machine, 'Helvetica', delay)

        while 1:
            if io.input(pir_pin):
                lcd.show()
            time.sleep(1)

    except KeyboardInterrupt:
        print "\nExiting..."
    except Exception, e:
        print(e)
    finally:
        io.cleanup()

if __name__ == '__main__':
    sys.exit(main())

