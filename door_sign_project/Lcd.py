__author__ = 'mqs'

import os
import time
import DoorSignApi


class NeoSec():
    frame_buffer = 1

    def __init__(self, machine_id, font, delay):
        self.machine_id = machine_id
        self.font = font
        self.delay = delay

    def show(self):
        active = DoorSignApi.ActiveMessage(self.machine_id)
        active.request()

        for item in active.response.json()['data']:
            if item['type'] == 'image':
                self.show_image(item['image'])
            elif item['type'] == 'text':
                self.show_text(item['text'])
            else:
                raise ValueError(
                    'Message type from json response is not allowed', self)

    def show_image(self, image):
        api = DoorSignApi.Image(image['id'])
        api.request()
        fh = open("tmp/message", "wb")
        fh.write(api.response.read())
        fh.close()

        os.system('fbi -d /dev/fb%d -T 1 -noverbose -a tmp/message' % self.frame_buffer)
        self.screen_on()
        time.sleep(self.delay)
        self.screen_off()
        os.system('kill $(pgrep fbi)')

    def show_text(self, text):
        command = ('convert -background black '
                   '-fill white '
                   '-font %s '
                   '-size 480x320 '
                   '-gravity Center '
                   'label:"%s" '
                   'tmp/text.gif')

        os.system(command % (self.font, text['text']))
        os.system('fbi -d /dev/fb%d -T 1 -noverbose -a tmp/text.gif' % self.frame_buffer)
        self.screen_on()
        time.sleep(self.delay)
        self.screen_off()
        os.system('kill $(pgrep fbi)')
        os.remove('tmp/text.gif')

    def screen_on(self):
        os.system('echo 0 | tee /sys/class/backlight/fb_tinylcd/bl_power')

    def screen_off(self):
        os.system('echo 1 | tee /sys/class/backlight/fb_tinylcd/bl_power')
