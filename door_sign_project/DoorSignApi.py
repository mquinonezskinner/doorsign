__author__ = 'mqs'

import requests
import urllib2


class API(object):
    header = 'X-Api-Key'
    key = 'YOURAPIKEY'
    host = 'http://example.org'
    api_path = '/api_path'

    def __init__(self, section):
        self.section = section
        self.response = None

    def request(self):
        try:
            headers = {self.header: self.key}
            self.response = requests.get(
                self.host + self.api_path + self.section,
                headers=headers)
            self.raise_exception()
        except Exception as e:
            print(e)

    def raise_exception(self):
        try:
            self.response.raise_for_status()
        except Exception as e:
            print(e)


class ActiveMessage(API):
    def __init__(self, machine_id):
        super(ActiveMessage, self).__init__(
            '/machine/%d/active' % machine_id)


class Image(API):
    def __init__(self, image_id):
        super(Image, self).__init__('/image/%d' % image_id)

    def request(self):
        try:
            request = urllib2.Request(self.host + self.api_path + self.section)
            request.add_header(self.header, self.key)
            self.response = urllib2.urlopen(request)
        except Exception as e:
            print(e)
