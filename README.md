# README #

This README would normally document whatever steps are necessary to get your application up and running.

This module assumes RPi.GPIO.setmode of BOARD. You may change it to *BCM*, if that's what you prefer.

## Configuration ##
###Required ###
**DoorSignApi.py**
```
#!python

    key      = 'YOURAPIKEY'
    host     = 'http://example.org'
    api_path = '/api_path'
```

### Optional Changes ###
**launch.py**
```
#!python

    pir_pin  = 7   # (int) Which pin are you using.
    delay    = 10  # (int) How long to display the image
    machine  = 2   # (int) Machine ID from front end Symfony app
```

## Dependencies ##
### Python ###
1. RPi.GPIO
1. urllib2
1. requests

### System ###
1. imagemagick

## Deployment instructions ##
* Easiest way is to checkout repo, set up a virtualenv and then install python dependencies with pip
* The python script needs to be run as root because it makes calls to NeoSec's library and framebuffer.

```
    [dev]$ su
    [dev]# virutualenv door_sign
    [dev]# cd door_sign
    [dev]# source bin/activate
    [dev]# pip install -r requirements.txt
    [dev]# cd door_sign_project
    [dev]# python launch.py
```


### Who do I talk to? ###
* Miguel Quinonez-Skinner mqs@berkeley.edu
* John Keller jkeller@berkeley.edu